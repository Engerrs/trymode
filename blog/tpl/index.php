<!DOCTYPE html>
<html>
<head lang="en">
    <?php
        include_once("header.php");
        ?>
</head>
<body>
    <h1 class="main-title">My Blog</h1>
    <div class="container-fluid">
        <div class="col-md-3">
            <?php
                require_once('menu.php');
                
            ?>
            <?php
                require_once('music/search.php');
                
            ?>
        </div>
        <div class="col-md-9 blog-body">

            <?php foreach($arline as $line) {?>

            <div class="post">
                <h2 class="post-title"> <?php echo $line['title'] ?> </h2>
                <h3 class="post-subtitle">
                    <?php echo $line['summary'] ?>
                </h3>
                    
                <p class="post-meta"><span class="glyphicon glyphicon-time"></span> Posted by <a href="#">Start Bootstrap</a> <?php echo $line['date'] ?>
                    <a href="post.php?id=<?php echo $line['id'] ?>" class="btn btn-primary btn-sm pull-right">Read More</a>
                </p>
                
                <hr />
            </div>
            
            <?php } ?>
            
            
            <ul class="pagination pull-right" boundary-links="true">
                <?php 
                    if ($page > 1) {
                    $prev = $page - 1;
                    echo "<li><a href='?page=1' class='ng-binding'>First</a></li>";
                    echo "<li><a href='?page={$prev}' class='ng-binding'>Previous</a></li>";
                } 
                else {
                    if ($pages !== 0) {
                        echo "<li class='ng-scope disabled'><a href='?page=' class='ng-binding'>First</a></li>";
                        echo "<li class='ng-scope disabled'><a href='?page=' class='ng-binding'>Previous</a></li>";
                    }
                }
                 for ($i = $page - 3; $i < $page + 3; $i++) {
                    if ($i > 0 && $i <= $pages) {
                        if ($i == $page) {
                            echo "<li class='active'><a href='?page={$i}' class='ng-binding'>" . $i . "</a></li>";
                        }
                        else {
                            echo "<li><a href='?page={$i}' class='ng-binding'>" . $i . "</a></li>";
                        }
                    }
                }
                     if ($page < $pages) {
                    $next = $page + 1;
                    echo "<li><a href='?page=$next'>Next</a></li>";
                    echo "<li><a href='?page=$pages'>Last</a></li>";
                } 
                else {
                    if ($pages !== 0) {
                        echo "<li class='ng-scope disabled'><a href='?page=$pages'>Next</a></li>";
                        echo "<li class='ng-scope disabled'><a href='?page=$pages'>Last</a></li>";
                    }
                }
                ?>
            </ul>
        </div>    
        
    </div>
</body>
</html>