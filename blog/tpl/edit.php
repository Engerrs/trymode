<!DOCTYPE html>
<html>
<head lang="en">
     <?php
        include_once("header.php");
        ?>
</head>
<body>
    <h1 class="main-title">My Blog</h1>
    <div class="container-fluid">
        <div class="col-md-3">
          <?php
                require_once('menu.php');
            ?>
        </div>
        <div class="col-md-9 blog-body">
        
            <div class="post">
                
                <h1>Change blog info</h1>
                
                
                <form  action="" method='post'>
                    <input type="hidden" name="id" value="<?php echo $post_data['id'] ?>">
                    <div class="form-group">
                        <label>Post title</label>
                        <input type="text" class="form-control" name="title" value="<?php echo $post_data['title'] ?>" />
                    </div>
                    
                    <div class="form-group">
                        <label>Post summary</label>
                        <textarea class="form-control" name="summary"><?php echo $post_data['summary'] ?></textarea>
                    </div>
                    
                    <div class="form-group">
                        <label>Post body</label>
                        <textarea rows="10" class="form-control" name="body"><?php echo $post_data['body'] ?></textarea>
                    </div>
                    
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary form-control" value="Edit" />
                    </div>
                </form>
                
                
                <hr />
            </div>
            
        </div>    
        
    </div>
</body>
</html>