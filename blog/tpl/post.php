<!DOCTYPE html>
<html>
<head lang="en">
     <?php
        include_once("header.php");
        ?>
</head>
<body>
    <h1 class="main-title">My Blog</h1>
    <div class="container-fluid">
        <div class="col-md-3">
           <?php
                require_once('menu.php');

            ?>
            <?php
                require_once('music/search.php');
                
            ?>
        </div>
        <div class="col-md-9 blog-body">
                
            <div class="post">
                <h2 class="post-title"><?php echo $arg['title'] ?></h2>
                <h3 class="post-subtitle">
                    <?php echo $arg['summary'] ?>
                </h3>
                <p class="post-subtitle" style="color: red;">
                    <?php echo $arg['body'] ?>
                </p>
                    
                <p class="post-meta"><span class="glyphicon glyphicon-time"></span> Posted by <a href="#">Start Bootstrap</a> <?php echo $arg['date'] ?>
                </p>
                <?php
                if(isset($_SESSION['authkey'])){ ?>
                        <a href="delete.php?id=<?php echo $line['id'] ?>">Delete Post</a>
                        <br />
                        <a href="edit.php?id=<?php echo $line['id'] ?>">Edit</a>
               <?php }
               ?>
            </div>
            

        </div>    
        
    </div>
</body>
</html>