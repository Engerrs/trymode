<!DOCTYPE html>
<html>
<head lang="en">
     <?php
        include_once("header.php");
        ?>
</head>
<body>
    <h1 class="main-title">My Blog</h1>
    <div class="container-fluid">
        <div class="col-md-3">
            <?php
                require_once('menu.php');
            ?>
            <?php
                require_once('music/search.php');
                
            ?>
        </div>
        <div class="col-md-9 blog-body">
        
            <div class="col-lg-6 col-lg-offset-3 ng-scope">
            
            
                <div class="panel panel-success" style="margin-top:20px;">
                    <div class="panel-heading">
                        <h2 style="margin:0;" class="ng-binding">Login</h2>
                    </div>
                    <div class="panel-body">
                
                        
                            <form ng-submit="loginForm.submit()" novalidate="" name="loginFrm" class="ng-pristine ng-valid-email ng-invalid ng-invalid-required" method='POST'>
                    <?php
                    if (isset($error))
                        echo 'Auth Error'. '<br>';


                    if(isset($_SESSION['authkey']))
                        echo 'login sucsses';
                    ?>
                                <div class="form-group required ">
                                    <label class="control-label ng-binding" for="loginform-email">Username</label>
                                    <input type="text" id="loginform-email" class="form-control ng-pristine ng-untouched ng-valid-email ng-invalid ng-invalid-required" name="username" ng-model="loginForm.email" ng-required="true" required="required">
                                </div>
                    
                                <div class="form-group required ">
                                    <label class="control-label ng-binding" for="loginform-password">Password</label>
                                    <input type="password" id="loginform-password" class="form-control ng-pristine ng-untouched ng-invalid ng-invalid-required" name="password" ng-model="loginForm.password" ng-required="true" required="required">
                                </div>
                    
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="remember-me" name="rememberMe" ng-model="loginForm.rememberMe" class="ng-pristine ng-untouched ng-valid"> Keep me logged in
                                    </label>
                                </div>
                    
                                <div class="form-group">
                                    <input type="submit" id="login-button" class="btn btn-primary form-control" value="Login">
                                     
                                </div>
                                <div class="form-group">
                                <a class="btn btn-primary form-control"
                                href='https://oauth.vk.com/authorize?client_id=5063815&display=page&redirect_uri=http://localhost/trymode/blog/vkauth.php&scope=email,offline,wall,friends&response_type=code&v=5.37'>VK</a>

                                </div>

                
                            </form>
                
                    </div>
                </div>
            </div>
        </div>    
        
    </div>
</body>
</html>