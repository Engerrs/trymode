<?php

$config = require_once('config.php');

if(isset($_SESSION['authkey']))
{
	header('Location: index.php');
	unset($_SESSION['authkey']);
}
if(!isset($_SESSION['authkey']))
	{
		header('Location: index.php');
	}