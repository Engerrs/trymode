<?php
$config = include_once('config.php');

$filename = 'db/posts.json';
$f = fopen($filename,'r+');
$arline = [];

if(isset($_SESSION['authkey']) && isset($_GET['id'])){
while (!feof($f) ) {
	$arline[] = json_decode(fgets($f),true);
}

foreach($arline as $id => $line) {
	$arr = array_search($_GET['id'], $line);
	if($arr) {
		$key = $id;
		
		break;
	}
}
$body = file_get_contents('db/' . $_GET['id'] . '.json');
$arline[$key] = array_merge($arline[$key], array('body' => json_decode($body)));
$post_data = $arline[$key];

if (isset($_POST) && !empty($_POST)) {

	$arline[$key] = [
		'id'=> $_POST['id'],
		'title' => $_POST['title'],
		'summary' => $_POST['summary'],
		'date' => date('d.m.Y H:i:s')
	];
	file_put_contents('db/'. $_POST['id'] . '.json', json_encode($_POST['body']));
	ftruncate($f, 0);
	$count = count($arline) - 1;
	foreach ($arline as $id => $line) {

		$str = ($count == $id) ? json_encode($line, true) : json_encode($line, true) . PHP_EOL;
		file_put_contents($filename, $str, FILE_APPEND);
	}
	header('Location: post.php?id='.$_GET['id']);
}

fclose($f);
}
require_once('tpl/edit.php');