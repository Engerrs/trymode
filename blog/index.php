<?php

$config = include_once("config.php");
$arline = [];
$filename = "db/posts.json";
$pageSize = $config["pageSize"];
$page = 1;
$pages = 0;

if (file_exists($filename)) {
    if (isset($_GET['page']) && is_numeric($_GET['page']) && $_GET['page'] > 0) {
            $page = $_GET['page'];
        }

    if (file_exists($filename)) {
        $f = fopen($filename, "r");

        $offset = ($page - 1) * $pageSize;
        $end = $offset + $pageSize;
        for ($i = 0; $i < $offset; $i++) {
            fgets($f);
        }

        for ($i = $offset; ($i < $end && !feof($f)); $i++) {
            $file_info = json_decode(fgets($f), true);
            if (!is_null($file_info)) {
                $arline[] = $file_info;
            }
        }
    }
    $size = count(file($filename));
    $pages = ceil($size/$pageSize);
}
include_once("tpl/index.php");