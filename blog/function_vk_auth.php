<?php
//FUNCTIONS FOR OAuth VK//
/////////////////////////////////////////////////////////////////////////////////
function GetVKAccessToken($client_id,$client_secret,$redirect_uri) {
if (isset($_GET['code'])) {
//var_dump($_GET['code']);
$ch = curl_init();
curl_setopt_array($ch, [
CURLOPT_URL => 'https://oauth.vk.com/access_token',
CURLOPT_RETURNTRANSFER => true,
CURLOPT_POST => true,
CURLOPT_SSL_VERIFYPEER => false,
CURLOPT_SSL_VERIFYHOST => false,
CURLOPT_POSTFIELDS => [
"client_id" => $client_id,
"client_secret" => $client_secret,
"code" => $_GET['code'],
"redirect_uri" => $redirect_uri
],
]);
$a = curl_exec($ch);
//var_dump($a);
return $a;
}	
}
///////////////////////////////////////////////////////////////////////////////////
function GetVKPostFromWall(){
	$ch = curl_init();
    curl_setopt_array($ch, [
        CURLOPT_URL => 'https://api.vk.com/method/wall.get',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POST => true,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => false,
        CURLOPT_POSTFIELDS => [
            "owner_id" => $_SESSION['user_id'],
            "access_token" => $_SESSION['token'],
            "count" => 10,
            "filter" => "all"
        ],
    ]);

    $wallPosts = curl_exec($ch);
    $wallPosts = json_decode($wallPosts, true);
    //var_dump($wallPosts);
    return $wallPosts;
}