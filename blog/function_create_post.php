<?php

function CreatePost($fsize,$file_json){
	if(isset($_POST['summary']) && isset($_POST['title']) && isset($_POST['body'])) {
		$id = sha1(time() . mt_rand(0, 10000));
		$array_json = [
			"id" => $id,
			"title" => $_POST['title'],
			"summary" => $_POST['summary'],
			"date" => date('d.m.Y H:i:s')
		];
		$str = ($fsize > 0) ? PHP_EOL . json_encode($array_json) : json_encode($array_json);
		file_put_contents($file_json, $str, FILE_APPEND);
		$array_json['body'] = $_POST['body'];
		file_put_contents('db/'. $id . '.json', json_encode($array_json['body']));
	}
}