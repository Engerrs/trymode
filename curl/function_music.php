<?php
session_start();
//FUNCTIONS FOR http://api.pleer.com/token.php//
///////////////////////////////////////////////////////////////////////////////////

function GetTokenAccess($url,$UserPWD,$Post_array) {

	$ch = curl_init($url); // инициализация адресса
curl_setopt_array($ch, [
    CURLOPT_RETURNTRANSFER => true, // TRUE для возврата результата передачи в качестве строки из curl_exec() вместо прямого вывода в браузер.
    CURLOPT_POST => true, //TRUE для использования обычного HTTP POST
    CURLOPT_USERPWD => $UserPWD, // Логин и пароль, используемые при соединении, указанные в формате "[username]:[password]".
    //Инфа логина и пароля -> http://pleer.com/account/clients/add Идентификатор:Ключ.
    CURLOPT_POSTFIELDS => $Post_array,
]);

$a = curl_exec($ch);
return $a;
}

///////////////////////////////////////////////////////////////////////////////////

function GetMusicTopList($url,$AccessToken) {
if($data = $AccessToken) { // Вытаскиваем токен и заносим его в переменную.
	$data = json_decode($data, true);
	$token = $data['access_token'];

	
	$ch = curl_init($url);
	curl_setopt_array($ch, [ // Задаем опять параметры для нахождения треков
		CURLOPT_URL => 'http://api.pleer.com/index.php?access=$token', //адресс
		CURLOPT_HTTPHEADER => ['Authorization: Bearer ' . $token,] , //При обращении к любому методу общения с сервисом вы обязаны предоставить корректный токен доступа (Access Token), который может быть передан в HTTP-заголовке Authorization (The Authorization Request Header Field), в теле запроса (Form-Encoded Body Parameter) или в качестве параметра самого запроса (URI Query Parameter)
		CURLOPT_POST => true, //TRUE для использования обычного HTTP POST
		CURLOPT_RETURNTRANSFER => true, // TRUE для возврата результата передачи в качестве строки из curl_exec() вместо прямого вывода в браузер.
		CURLOPT_POSTFIELDS => [
			'method' => 'get_top_list', // метод, что нам надо вообще. В данном случае топ за Н промежуток.
			'language' => 'en', // соответственно язык.
			'page' => 1, // на странице 1.
			'list_type' => 1, // за какой период.
		],
	]);
	
	if($tracks = curl_exec($ch)) { // раскадируем трэки.
		$tracks = json_decode($tracks, true); // получаем в виде массива.
		return $tracks;
	}
}
}
///////////////////////////////////////////////////////////////////////////////////

function GetMusicDownloadLink($url) {
	$token = $_SESSION['token_music']; // Заносим токен.
$url = "http://api.pleer.com/index.php"; // адресс.
if (isset($_GET['id'])) { // достаем айди песни через ГЕТ.
	$id = $_GET['id']; // вставляем в переменную.
}

$ch = curl_init($url); // инициализация адресса.
curl_setopt_array($ch, [ // задаем параметры.
	CURLOPT_POST => true,
	CURLOPT_POSTFIELDS =>[ 
	"grant_type" => 'token',
	'method' => 'tracks_get_download_link', // получить ссылку на загрузку.
	'track_id' => $id, // (string, обязательный) идентификатор трека.
	'reason' => 'save'], // reason (string, обязательный) цель обращения. Должна быть равна listen (загрузка для прослушивания) или save (загрузка для сохранения)
	CURLOPT_HTTPHEADER => [
	'Authorization: Bearer '.$token],
	CURLOPT_RETURNTRANSFER => true]
	);

$song = curl_exec($ch); // Выполняет запрос cURL. Эта функция должна вызываться после инициализации сеанса и установки всех необходимых параметров.

$song = json_decode($song,true); 
	//var_dump($song);
return $song;
}
///////////////////////////////////////////////////////////////////////////////////

function GetMusicSearch($url){
	$ch = curl_init($url); // инициализация адресса
curl_setopt_array($ch, [
    CURLOPT_RETURNTRANSFER => true, // TRUE для возврата результата передачи в качестве строки из curl_exec() вместо прямого вывода в браузер.
    CURLOPT_POST => true, //TRUE для использования обычного HTTP POST
    CURLOPT_USERPWD => '283426:YO2Ra4VTJ9Vx6bRGBNxT', // Логин и пароль, используемые при соединении, указанные в формате "[username]:[password]".
    //Инфа логина и пароля -> http://pleer.com/account/clients/add Идентификатор:Ключ.
    CURLOPT_POSTFIELDS => [ // Все данные, передаваемые в HTTP POST-запросе
        "grant_type" => "client_credentials"
    ],
]);
$a = curl_exec($ch);// Получаем токен. Выполняет запрос cURL. Эта функция должна вызываться после инициализации сеанса и установки всех необходимых параметров.
//echo $a;	
	if($data = curl_exec($ch)) { // Вытаскиваем токен и заносим его в переменную.
	$data = json_decode($data, true);
	$token = $data['access_token'];
}
//var_dump($token);
if(isset($_POST['artist']) or isset($_POST['track'])) { // Задаем условия.
	if (isset($_POST['artist'])) {
		$artist = $_POST['artist'];
	}
	if (isset($_POST['track'])) {
		$track = $_POST['track'];
	} 
	if(empty($_POST['artist']) && empty($_POST['track'])){
		echo 'Запись не найдена.';
	}
curl_setopt_array($ch, [
	CURLOPT_URL => 'http://api.pleer.com/index.php', //адресс // задаем параметры.
	CURLOPT_HTTPHEADER => [
	'Authorization: Bearer '.$token,], //При обращении к любому методу общения с сервисом вы обязаны предоставить корректный токен доступа (Access Token), который может быть передан в HTTP-заголовке Authorization (The Authorization Request Header Field), в теле запроса (Form-Encoded Body Parameter) или в качестве параметра самого запроса (URI Query Parameter)
	CURLOPT_POST => true, //TRUE для использования обычного HTTP POST
	CURLOPT_POSTFIELDS =>[ 
	'method' => 'tracks_search', // Нахождения нужного.
	'query' => $artist.':'.$track,
	'result_on_page' => 10, // Отображения нахождения исполнитель:трэк.
	CURLOPT_RETURNTRANSFER => true]
	]);
if($tracks = curl_exec($ch)) { // раскадируем трэки.
		$tracks = json_decode($tracks, true); // получаем в виде массива.
		//var_dump($tracks);
	return $tracks;
}
}
}
///////////////////////////////////////////////////////////////////////////////////

?>