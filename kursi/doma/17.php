<?php

function foo() {
	echo 'foo';
	return true;
}

function bar() {
	echo 'bar';
	return false;
}

if (bar() && foo()) {
	echo '1';
}

echo '<br><br>';

if ( foo() || bar()) {
	echo '2';
}



?>


